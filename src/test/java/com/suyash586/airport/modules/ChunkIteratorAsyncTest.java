
package com.suyash586.airport.modules;

import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;

import com.suyash586.airport.modules.ChunkIteratorAsync;
import com.suyash586.airport.testcategory.Fast;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(Enclosed.class)
public class ChunkIteratorAsyncTest {
    @Category(Fast.class)
    public static class iterate {
        @Test
        public void Can_Iterate_String_List() {
            final Integer expectedResult = 2;
            final List<Integer> result = Arrays.asList(0);

            ChunkIteratorAsync.of(Arrays.asList("a", "b", "c", "d"))
                    .setSize(2)
                    .iterate(chunk -> {
                        result.set(0, result.get(0) + 1);
                    });

            assertEquals(expectedResult, result.get(0));
        }
    }
}
