
package com.suyash586.airport.config;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.SharedCacheMode;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DatabaseConfig {
    private final String dbDriver;
    private final String dbPassword;
    private final String dbUrl;
    private final String dbUsername;
    private final String hibernateDialect;
    private final String hibernateShowSql;

    private final String hibernateHbm2DdlAuto;
    private final String entitymanagerPackagesToScan;

    private final String hibernateFormatSql;

    private final String hibernateUseSqlComments;

    public DatabaseConfig(@Value("${spring.datasource.driver-class-name}") final String dbDriver,
                          @Value("${spring.datasource.url}") final String dbUrl,
                          @Value("${spring.datasource.username}") final String dbUsername,
                          @Value("${spring.datasource.password}") final String dbPassword,
                          @Value("${spring.jpa.hibernate.dialect}") final String hibernateDialect,
                          @Value("${spring.jpa.show_sql}") final String hibernateShowSql,
                          @Value("${spring.jpa.hibernate.ddl-auto}") final String hibernateHbm2DDLAuto,
                          @Value("${entitymanager.packagesToScan}") final String entitymanagerPackagesToScan,
                          @Value("${spring.jpa.properties.hibernate.use_sql_comments}") final String hibernateUseSqlComments,
                          @Value("${spring.jpa.properties.hibernate.format_sql}") final String hibernateFormatSql) {
        this.dbDriver = dbDriver;
        this.dbPassword = dbPassword;
        this.dbUrl = dbUrl;
        this.dbUsername = dbUsername;
        this.hibernateDialect = hibernateDialect;
        this.hibernateShowSql = hibernateShowSql;
        this.hibernateHbm2DdlAuto = hibernateHbm2DDLAuto;
        this.entitymanagerPackagesToScan = entitymanagerPackagesToScan;
        this.hibernateFormatSql = hibernateFormatSql;
        this.hibernateUseSqlComments = hibernateUseSqlComments;
    }
    @Bean
    public EntityManager entityManager() {
        return entityManagerFactory(dataSource(), hibernateProperties()).createEntityManager();
    }

    @Bean
    public EntityManagerFactory entityManagerFactory(final DataSource dataSource, final Properties hibernateProperties) {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(entitymanagerPackagesToScan);
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(hibernateProperties);
        em.setPersistenceUnitName("airportdb");
        em.setSharedCacheMode(SharedCacheMode.ENABLE_SELECTIVE);
        em.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        em.afterPropertiesSet();

        return em.getObject();
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dbDriver);
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(dbUsername);
        dataSource.setPassword(dbPassword);
        return dataSource;
    }

    @Bean
    public Properties hibernateProperties() {
        final Properties properties = new Properties();

        properties.put("hibernate.dialect", hibernateDialect);
        properties.put("hibernate.globally_quoted_identifiers", "true");
        properties.put("hibernate.show_sql", hibernateShowSql);
        properties.put("hibernate.use_sql_comments", hibernateUseSqlComments);
        properties.put("hibernate.format_sql", hibernateFormatSql);
        properties.put("hibernate.hbm2ddl.auto", hibernateHbm2DdlAuto);
        properties.put("hibernate.connection.driver_class", dbDriver);

        properties.put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
        properties.put("hibernate.cache.use_second_level_cache", "true");
        properties.put("hibernate.cache.use_query_cache", "true");


        properties.put("hibernate.search.default.directory_provider", "filesystem");
        properties.put("hibernate.search.default.indexBase", "./lucene_indexes/");

        return properties;
    }
}
