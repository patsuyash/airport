
package com.suyash586.airport.modules;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Data
public class ChunkIterator<T extends Iterable<U>, U> {
	static public <T extends Iterable<U>, U> ChunkIterator<T, U> of(final T iterable) {
		return new ChunkIterator<>(iterable);
	}

	final private List<U> chunk = new ArrayList<>();

	final private T iterable;
	private int size = 100;

	protected ChunkIterator(final T iterable) {
		this.iterable = iterable;
	}

	public void iterate(final Consumer<List<U>> consumer) {
		for (final U iteration : getIterable()) {
			getChunk().add(iteration);

			if (ifChunkIsFull()) {
				processChunk(consumer);
			}
		}

		processChunk(consumer);
	}

	protected boolean ifChunkIsFull() {
		return getChunk().size() >= getSize();
	}

	protected void processChunk(final Consumer<List<U>> consumer) {
		if (getChunk().size() > 0) {
			consumer.accept(getChunk());
		}

		getChunk().clear();
	}
}
