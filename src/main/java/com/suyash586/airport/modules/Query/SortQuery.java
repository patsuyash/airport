
package com.suyash586.airport.modules.Query;

import org.apache.lucene.search.Query;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.hibernate.search.SearchFactory;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class SortQuery {

    @PersistenceContext
    private EntityManager entityManager;

    public FullTextQuery parse(final Class clazz, final SortField... fields) {
        final QueryBuilder builder = getSearchFactory().buildQueryBuilder().forEntity(clazz).get();

        final Query query = builder.all().createQuery();
        final Sort sort = new Sort(fields);

        return getFullTextEntityManager().createFullTextQuery(query, clazz).setSort(sort);
    }

    private SearchFactory getSearchFactory() {
        return getFullTextEntityManager().getSearchFactory();
    }

    private FullTextEntityManager getFullTextEntityManager() {
        return Search.getFullTextEntityManager(entityManager);
    }
}
