
package com.suyash586.airport.modules;

import org.springframework.scheduling.annotation.Async;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

public class ChunkIteratorAsync<T extends Iterable<U>, U> extends ChunkIterator<T, U> {
	static public <T extends Iterable<U>, U> ChunkIteratorAsync<T, U> of(final T iterable) {
		return new ChunkIteratorAsync<>(iterable);
	}

	private ChunkIteratorAsync(final T iterable) {
		super(iterable);
	}

	@Override
	final public void iterate(final Consumer<List<U>> consumer) {
		final List<CompletableFuture<Void>> futures = new ArrayList<>();
		for (final U iteration : getIterable()) {
			getChunk().add(iteration);

			if (ifChunkIsFull()) {
				futures.add(processChunk(consumer, getChunk()));
			}
		}
		futures.add(processChunk(consumer, getChunk()));

		CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
	}

	@Async
	private CompletableFuture<Void> processChunk(final Consumer<List<U>> consumer, final List<U> chunk) {
		final List<U> chunkBlock = new ArrayList<>(chunk);
		chunk.clear();

		return CompletableFuture.runAsync(chunkBlock.size() > 0 ? () -> consumer.accept(chunkBlock) : () -> {
		});
	}
}
