
package com.suyash586.airport.modules.CSVParser;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVRecord;

import com.suyash586.airport.models.core.BaseEntity;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
abstract public class CSVParser<T extends BaseEntity> implements CSVParserStrategy {
    final public void parse(final CSVRecord record) {
        if (record != null && checkValidity(record)) {
            Optional.ofNullable(process(CSVEntity.of(record)))
                    .ifPresent(this::save);
        } else {
            logger.error("record not read {}", record);
        }
    }

    private Boolean checkValidity(final CSVRecord record) {
        return record.isConsistent();
    }

    abstract public T process(final CSVEntity record);

    final public void parseAll(final List<CSVRecord> records) {
        save(records.stream()
                .filter(this::checkValidity)
                .map(this::process)
                .collect(Collectors.toList()));
    }

    abstract protected void save(final List<T> item);

    private T process(final CSVRecord record) {
        return process(CSVEntity.of(record));
    }

    abstract protected void save(final T item);
}
