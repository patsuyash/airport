
package com.suyash586.airport.modules.CSVParser;

import lombok.Data;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Optional;

@Data
public class CSVEntity {
	public static CSVEntity of(final CSVRecord record) {
		return new CSVEntity(record);
	}

	private final CSVRecord record;

	private CSVEntity(final CSVRecord record) {
		this.record = record;
	}

	public Double getDouble(final String key) {
		return getOptional(key).filter(NumberUtils::isCreatable).map(Double::parseDouble).orElse(null);
	}

	public Optional<String> getOptional(final String key) {
		return Optional.ofNullable(get(key));
	}

	public String get(final String key) {
		return record.get(key);
	}

	public Integer getInt(final String key) {
		return getOptional(key).filter(NumberUtils::isCreatable).map(Integer::parseInt).orElse(null);
	}

	public Long getLong(final String key) {
		return getOptional(key).filter(NumberUtils::isCreatable).map(Long::parseLong).orElse(null);
	}
}
