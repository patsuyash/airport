
package com.suyash586.airport.modules.CSVParser;

import org.apache.commons.csv.CSVRecord;

public interface CSVParserStrategy {
    void parse(final CSVRecord record);
}
