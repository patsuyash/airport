
package com.suyash586.airport.components;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Component
@Slf4j
public class BuildSearchIndex implements ApplicationListener<ApplicationReadyEvent>, Ordered {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public final void onApplicationEvent(final ApplicationReadyEvent event) {
        try {
            final FullTextEntityManager fullTextEntityManager =
                    Search.getFullTextEntityManager(entityManager);
            fullTextEntityManager.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            logger.info("Search Index Build  Failure", e);
        }
    }
    @Override
    public int getOrder() {
        return LOWEST_PRECEDENCE;
    }
}