
package com.suyash586.airport.view;

import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.suyash586.airport.models.country.Country;
import com.suyash586.airport.models.country.CountryService;

import java.util.Optional;

@Controller
public class QueryController {

    private final CountryService countryService;

    @Autowired
    public QueryController(final CountryService countryService) {
        this.countryService = countryService;
    }

    @RequestMapping(value = "/query/", params = {"q"}, method = RequestMethod.GET)
    public String queryCountryEmpty(@RequestParam("q") final String queryText,
                                    final Model model) throws ParseException {
        return queryCountry(queryText, model);
    }

    @RequestMapping(value = "/query/{query}", method = RequestMethod.GET)
    public String queryCountry(@PathVariable("query") final String queryString, final Model model) throws ParseException {
        final Optional<Country> countryOptional = countryService.queryCountry(queryString);

        countryOptional.ifPresent(country -> {
            model.addAttribute("country", country);
            model.addAttribute("searchTerm", queryString);
        });

        return "pages/query";
    }

    @RequestMapping(value = "/query/", method = RequestMethod.GET)
    public String queryCountryEmpty(final Model model) throws ParseException {
        return queryCountry(" ", model);
    }
}
