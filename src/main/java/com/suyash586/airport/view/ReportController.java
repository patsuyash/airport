
package com.suyash586.airport.view;

import com.suyash586.airport.models.country.CountryService;
import com.suyash586.airport.models.runway.RunwayService;

import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ReportController {

    private final CountryService countryService;

    private final RunwayService runwayService;

    @Autowired
    public ReportController(final CountryService countryService,
                            final RunwayService runwayService) {
        this.countryService = countryService;
        this.runwayService = runwayService;
    }

    @RequestMapping(value = "/report/airport", method = RequestMethod.GET)
    public String reportAirport(final Model model) throws ParseException {
        model.addAttribute("highestAirports", countryService.topCountriesInAirportCount());
        model.addAttribute("lowestAirports", countryService.lowestCountriesInAirportCount());

        return "pages/airportstats";
    }

    @RequestMapping(value = "/report/surface", method = RequestMethod.GET)
    public String reportSurface(final Model model) throws ParseException {
        model.addAttribute("statistics", countryService.getAllSurfaceStatistics());

        return "pages/surfacestats";
    }

    @RequestMapping(value = "/report/runway", method = RequestMethod.GET)
    public String reportRunways(final Model model) throws ParseException {
        model.addAttribute("statistics", runwayService.topRunwayIdentifications());

        return "pages/runwaystats";
    }
}
