
package com.suyash586.airport.models.country;

import java.util.List;
import java.util.Optional;

import com.suyash586.airport.models.runway.SurfaceTypeStats;

public interface CountryService {
    Optional<Country> queryCountry(final String queryString);

    List<Country> topCountriesInAirportCount();

    List<Country> lowestCountriesInAirportCount();

    List<SurfaceTypeStats> getSurfaceStatistics(final Country country);

    List<CountrySurfaceTypeStats> getAllSurfaceStatistics();
}
