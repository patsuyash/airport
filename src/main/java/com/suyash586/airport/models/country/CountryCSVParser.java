
package com.suyash586.airport.models.country;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.suyash586.airport.modules.CSVParser.CSVEntity;
import com.suyash586.airport.modules.CSVParser.CSVParser;

import java.util.List;

@Component
public class CountryCSVParser extends CSVParser<Country> {
    private final CountryRepository countryRepository;

    @Autowired
    public CountryCSVParser(final CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public Country process(final CSVEntity record) {
        return Country.builder()
                .code(record.get("code"))
                .name(record.get("name"))
                .continent(record.get("continent"))
                .id(record.getLong("id"))
                .keywords(record.get("keywords"))
                .wikipediaLink(record.get("wikipedia_link"))
                .build();
    }

    @Override
    protected void save(final List<Country> items) {
        countryRepository.save(items);
    }

    @Override
    protected void save(final Country item) {
        countryRepository.save(item);
    }
}
