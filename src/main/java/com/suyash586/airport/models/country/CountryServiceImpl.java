
package com.suyash586.airport.models.country;

import com.suyash586.airport.models.runway.SurfaceTypeStats;
import com.suyash586.airport.modules.Query.PhraseQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CountryServiceImpl implements CountryService {

    private final PhraseQuery phraseQuery;

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(final PhraseQuery phraseQuery,
                              final CountryRepository countryRepository) {
        this.phraseQuery = phraseQuery;
        this.countryRepository = countryRepository;
    }

    @Override
    public Optional<Country> queryCountry(final String queryString) {
        @SuppressWarnings("unchecked") final List<Country> countries = phraseQuery.parse(Country.class, queryString, new String[]{
                "code", "code.ngram", "code.edge", "name", "name.ngram", "name.edge"
        })
                .setMaxResults(1)
                .getResultList();

        return countries.isEmpty() ? Optional.empty() : Optional.ofNullable(countries.get(0));
    }

    @Override
    public List<Country> topCountriesInAirportCount() {
        return countryRepository.topCountriesInAirportCount();
    }

    @Override
    public List<Country> lowestCountriesInAirportCount() {
        return countryRepository.lowestCountriesInAirportCount();
    }

    @Override
    public List<SurfaceTypeStats> getSurfaceStatistics(final Country country) {
        return countryRepository.typeOfRunways(country.getId()).stream()
                .map(Object[].class::cast)
                .map(result -> SurfaceTypeStats.builder()
                        .surface(result[0].toString())
                        .count(Integer.parseInt(result[1].toString()))
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public List<CountrySurfaceTypeStats> getAllSurfaceStatistics() {
        return countryRepository.findAll().stream()
                .map(country -> CountrySurfaceTypeStats.builder()
                        .country(country)
                        .surfaceTypeStats(getSurfaceStatistics(country))
                        .build())
                .collect(Collectors.toList());
    }
}
