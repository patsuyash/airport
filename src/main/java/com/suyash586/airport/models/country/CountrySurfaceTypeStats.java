
package com.suyash586.airport.models.country;

import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.suyash586.airport.models.runway.SurfaceTypeStats;

@Data
public class CountrySurfaceTypeStats {
    private final Country country;
    private final List<SurfaceTypeStats> surfaceTypeStats;

    @Builder
    private CountrySurfaceTypeStats(final Country country,
                                    final List<SurfaceTypeStats> surfaceTypeStats) {
        this.country = country;
        this.surfaceTypeStats = Optional.ofNullable(surfaceTypeStats).orElse(new ArrayList<>());
    }
}
