
package com.suyash586.airport.models.runway;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RunwayServiceImpl implements RunwayService {
    private final RunwayRepository runwayRepository;

    @Autowired
    public RunwayServiceImpl(final RunwayRepository runwayRepository) {
        this.runwayRepository = runwayRepository;
    }

    @Override
    public List<RunwayIdentificationStats> topRunwayIdentifications() {
        return runwayRepository.topRunwayIdentifications().stream()
                .map(Object[].class::cast)
                .map(result -> RunwayIdentificationStats.builder()
                        .identification(result[0].toString())
                        .count(Integer.parseInt(result[1].toString()))
                        .build())
                .collect(Collectors.toList());
    }
}
