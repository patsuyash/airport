
package com.suyash586.airport.models.runway;

import java.util.List;

public interface RunwayService {
    List<RunwayIdentificationStats> topRunwayIdentifications();
}
