
package com.suyash586.airport.models.runway;

import lombok.Builder;
import lombok.Data;

import java.util.Optional;

@Data
public class RunwayIdentificationStats {
    private String identification;
    private Integer count;

    @Builder
    private RunwayIdentificationStats(final String identification,
                                      final Integer count) {
        this.identification = Optional.ofNullable(identification).orElse("unknown");
        this.count = Optional.ofNullable(count).orElse(0);
    }
}
