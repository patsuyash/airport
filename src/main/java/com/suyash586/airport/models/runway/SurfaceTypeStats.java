
package com.suyash586.airport.models.runway;

import lombok.Builder;
import lombok.Data;

import java.util.Optional;

@Data
public class SurfaceTypeStats {
    private String surface;
    private Integer count;

    @Builder
    private SurfaceTypeStats(final String surface,
                             final Integer count) {
        this.surface = Optional.ofNullable(surface).orElse("undefined");
        this.count = Optional.ofNullable(count).orElse(0);
    }
}
