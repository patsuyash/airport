
package com.suyash586.airport.models.airport;

import org.springframework.stereotype.Repository;

import com.suyash586.airport.models.core.BaseRepository;

@Repository
public interface AirportRepository extends BaseRepository<Airport> {
}
