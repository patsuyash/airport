package com.suyash586.airport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.suyash586.airport")
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
